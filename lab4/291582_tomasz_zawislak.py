#!/usr/bin/python3 python

import numpy
from matplotlib import pyplot as plt

A = numpy.array([[6,5,-5], [2,6,-2], [2,5,-1]])
print("Dana macierz: \n", A)
x = numpy.random.random_sample((3,))
x /= numpy.linalg.norm(x)
N = 20
numpy.set_printoptions(precision=4)
print("\nn\twar. wł.\twektor własny")
# tablice do wykresu:
norm=0
xx = []
yy = []
# obliczenia
for i in range(N):
    x = numpy.dot(A, x)
    norm = numpy.linalg.norm(x)
    x = x/norm
    print("%d\t%.4f"%(i+1,norm), "\t", x)
    xx.append(i+1)
    yy.append(norm)



print("\nWyznaczony wektor własny: ", x)
print("Wyznaczona wartość własna: %.4f"%(norm))
eigW, eigV = numpy.linalg.eig(A)
print("\nWartości własne z numpy: ", eigW)
print("Wektory własne z numpy: \n", eigV)

# Wykresy

plt.axhline(norm, color='black')
plt.xlabel('numer iteracji')
plt.ylabel('oszacowana wartość własna')
plt.plot(xx, yy, color='b')
plt.show()
