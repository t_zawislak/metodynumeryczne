"""
    Author: Tomasz Zawiślak
    Heat transport

"""

#!/bin/usr/python3 python
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm


D, L, NL, tk, Nt = 0.1, 1., 30, 1., 2500
dt = tk/Nt
dL = L/NL

def krok(u, t, i):
    return u[i] + D*dt/dL/dL*(u[i+1]-2*u[i]+u[i-1])

wszystkie_u = np.zeros(shape=(Nt, NL)) # inicjalizacja zerami
for i in range(1, NL-1):
    wszystkie_u[0][i] = np.sin(np.pi * i/NL)
for i in range(1, Nt):
    for j in range(1, NL-1):
        wszystkie_u[i][j] = krok(wszystkie_u[i-1], i*dt, j)

t = np.arange(0, tk, dt)
x = np.arange(0, L, dL)

fig = plt.figure()
ax3d = fig.add_subplot(121, projection='3d')
ax2d = fig.add_subplot(122)

X, Y = np.meshgrid(x, t)
ax2d.set_xlabel('długość L [m]')
ax2d.set_ylabel('czas t [s]')
ax2d.pcolormesh(x,t,wszystkie_u, cmap=cm.coolwarm)

ax3d.set_xlabel('długość L [m]')
ax3d.set_ylabel('czas t [s]')
ax3d.set_zlabel('Temperatura [K]')
ax3d.plot_surface(X, Y, wszystkie_u, cmap=cm.coolwarm)

plt.show()
