Numerical methods course:

lab2:
    finds roots of f(x) within an input-specified range
    
    Input:
    $ python3 lab2.py 0 2

lab3:
    LU decomposition of macierz.txt

lab4:
    eigenvalues and eigenvectors

lab5: 
    polynominal fit

lab6:
    Lorenz attractor

lab7:
    Heat transport
