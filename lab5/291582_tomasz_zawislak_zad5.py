"""
    Author: Tomasz Zawiślak
    polynominal fit

"""
#!/usr/bin/python3 python
import numpy as np
from matplotlib import pyplot as plt

def wielomian(x,m,a):
    sum=0
    for i in range(m+1):
        sum+=a[i]*np.power(x, i)
    return sum

x = [1,2,3,4,5]
y = [52,5,-5,-40,10]
n = len(x)
m = 3


A = np.zeros(shape=(m+1,m+1))
Y = np.zeros(shape=(m+1, 1))

for i in range(m+1) :
    Y[i] = np.sum( y*np.power(x, i) )
    for j in range(m+1):
        A[i,j] = np.sum( np.power(x, i+j) )
print(A)
print(Y)
A_ = np.linalg.inv(A)
a = A_@Y
print(a)

x_curve = np.arange(x[0] - 0.3*np.absolute(x[0]), x[-1]+0.3*np.absolute(x[-1]) , 0.01)
y_curve = [wielomian(i,m,a) for i in x_curve ]

plt.xlabel('x')
plt.ylabel('y')
plt.plot(x, y, marker='o', color='r', ls='')
plt.plot(x_curve, y_curve, color='green')
plt.show()
