"""
    Author: Tomasz Zawiślak
    finds roots of f(x) within an input-specified range
    
    Input:
    $ python3 lab2 0 2

"""
#!/usr/bin/python3 python
import sys
import numpy
from matplotlib import pyplot as plt

def f(x):
    x = float(x)
    return 3*numpy.power(x,3) - 4*numpy.power(x,2) - 1

def ZmienPrzedzial(a, b):
    b=float(b)
    a=float(a)
    x0=(b+a)/2
    if f(a)*f(x0) < 0:
        return a,x0
    elif f(x0)*f(b) < 0:
        return x0,b
    elif f(x0)==0 :
        print ('Znaleziony pierwiastek: {}'.format(x0))


# Sprawdzam parametry wejsciowe
if len(sys.argv) < 3 :
    print ('Podano za malo argumentow.')
    sys.exit()
a = sys.argv[1]
b = sys.argv[2]
if a == b:
    print ('Granice przedzialu musza byc rozne.')
    sys.exit()
if a > b:
    a,b=b,a
print ('Wybrany przedzial ({0}; {1})'.format(a,b))
if f(a)*f(b) >= 0:
    print ('Wybrany przedzial nie zawiera pierwiastka.')
    sys.exit()

# Poczatek programu:
a=float(a)
b=float(b)
epsilon = 0.00001
x_plot=[a,b]  # tablica punktow do pozniejszego narysowania

ia,ib=a,b
while abs(ia-ib)>epsilon:
    print ('Rozwazam przedzial ({0:.5f};{1:.5f})'.format(ia,ib))
    na,nb=ZmienPrzedzial(ia,ib)
    if na!=ia :
        x_plot.append(na)
    if nb!=ib :
        x_plot.append(nb)
    ia,ib=na,nb
print ('Znaleziony pierwiastek: {0:.5f} '.format((ia+ib)/2))

# Wykresy
x_curve = numpy.arange(a - 0.1*numpy.absolute(a), b*1.1 , 0.01)
y_curve = []
for i in x_curve:
    y_curve.append(f(i))

y_plot = []
for i in x_plot:
    y_plot.append(f(i))

plt.axhline(0, color='black')
plt.plot(x_curve, y_curve, color='b')
plt.plot(x_plot, y_plot, marker='o', color='r', ls='')
plt.show()
