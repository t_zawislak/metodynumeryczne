"""
    Author: Tomasz Zawiślak
    LU decomposition of macierz.txt

"""

#!/usr/bin/python3 python
import numpy as np
import sys
import scipy
import scipy.linalg

# zamieniam wczytane liczby na float
def make_float(num):
    num = num.strip(' ').strip(' ').strip('\n').replace(',','.').replace("−", "-")
    return float(num)

# sprawdzam czy macierz jest kwadratowa
def sprawdz_macierz(m):
    try:
        m.shape[0]
        m.shape[1]
    except:
        print("Wiersze lub kolumny nie zawierają równej liczby elementów. Proszę poprawić macierz w macierz.txt tak, by w każdym wierszu i każdej kolumnie znajdowało się tyle samo elementów. Proszę usunąć wszystkie białe znaki z końca pliku (po ostatnim elemencie macierzowym)")
        sys.exit()
    if m.shape[0] != m.shape[1] :
        print("Podana macierz nie jest kwadratowa.")
        sys.exit()
# funkcja pomocnicza
def suma_iloczynow(l, u, j, i, max) :
    suma=0
    for k in range(0, max):
        suma += l[j][k]*u[k][i]
    return suma

# dekompozycja LU
def rozloz_macierz(m) :
    n=m.shape[0]
    L=np.zeros((n,n))
    U=np.zeros((n,n))
    for i in range(0, n) :
        L[i][i] = 1
        for j in range(0, i+1) :
            U[j][i] = m[j][i] - suma_iloczynow(L, U, j, i, j)
        for j in range(i+1, n):
            L[j][i] = (m[j][i] - suma_iloczynow(L, U, j, i, i ))/U[i][i]
    return L, U



# wczytuję macierz z pliku macierz.txt

try:
	f = open("macierz.txt")
	with f:
		try:
			A = np.array([[make_float(x) for x in line.split()] for line in f])
			
			sprawdz_macierz(A)
			sL, sU = rozloz_macierz(A)
			print("\nWyjściowa macierz:\n")
			print(A)
			print("\nMacierze L i U otrzymane samodzielnie:\n")
			print("\nMacierz L:\n")
			print(sL)
			print("\nMacierz U:\n")
			print(sU)
			P, L, U = scipy.linalg.lu(A)
			print("\n=======================================================================")
			print("\nMacierze L i U otrzymane z scipy.linalg.lu:\n")
			print("\nMacierz L:\n")
			print(L)
			print("\nMacierz U:\n")
			print(U)
		# błąd przy wczytywaniu macierzy:
		except:
			print("Wczytywanie macierzy zakończone niepowodzeniem. Proszę sprawdzić czy macierz zawiera jedynie liczby.")
			sys.exit()
# brak pliku macierz.txt:
except FileNotFoundError:
	print("Nie znaleziono pliku macierz.txt")
	sys.exit()



