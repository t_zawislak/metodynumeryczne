"""
    Author: Tomasz Zawiślak
    Lorenz attractor

"""
#!/bin/usr/python3 python
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

w = [1,1,1]
t0 = 0
t_pocz = t0 # do tytułu wykresu
tk = 100
N_iter = 10000
h = (tk-t0)/N_iter

def dxdt(x, y, z):
    return 10*(y-x)
def dydt(x, y, z):
    return x*(28-z)-y
def dzdt(x, y, z):
    return x*y - 8./3.*z

def Kf1( dfdt, xi, yi, zi):
    return dfdt(xi, yi, zi)

def Kf2( dfdt, xi, yi, zi, K1):
    return dfdt(xi+0.5*K1[0]*h, yi+0.5*K1[1]*h, zi+0.5*K1[2]*h)

def Kf3( dfdt, xi, yi, zi, K2):
    return dfdt(xi+0.5*K2[0]*h, yi+0.5*K2[1]*h, zi+0.5*K2[2]*h)

def Kf4( dfdt, xi, yi, zi, K3):
    return dfdt(xi+K3[0]*h, yi+K3[1]*h, zi+K3[2]*h)

dfdts = [dxdt, dydt, dzdt]
coords = [[],[],[]]
# warunki początkowe
for i in range(3):
    coords[i].append(w[i])

while t0 <= tk :
    K = [[],[],[]]

    for i in range(3):  # licze wszystkie K1
        K[i].append( Kf1(dfdts[i], w[0], w[1], w[2]) )
    for i in range(3):  # licze wszystkie K2
        K1s = [K[0][0], K[1][0], K[2][0]]
        K[i].append( Kf2(dfdts[i], w[0], w[1], w[2], K1s) )
    for i in range(3):  # licze wszystkie K3
        K2s = [K[0][1], K[1][1], K[2][1]]
        K[i].append( Kf3(dfdts[i], w[0], w[1], w[2], K2s) )
    for i in range(3):  # licze wszystkie K4
        K3s = [K[0][2], K[1][2], K[2][2]]
        K[i].append( Kf4(dfdts[i], w[0], w[1], w[2], K3s) )
    # liczę współrzędne i+1
    for i in range(3):
        w[i] = w[i] + 1./6.*( K[i][0] + 2.*(K[i][1]+K[i][2]) + K[i][3] )*h
    # wpisuję wyniki
    for i in range(3):
        coords[i].append(w[i])

    t0 = t0 + h



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(coords[0], coords[1], coords[2], color="red")
ax.set_title("Rozwiązanie dla t $\epsilon$ <%.1i:%.1i> i %i jednostek czasu"%(t_pocz, tk, N_iter))
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
plt.show()
